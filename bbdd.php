<?php
// Función que recibe un idchef y devuelve su category
function getCategoryById($idchef) {
    $c = conectar();
    $select = "select category from chef where idchef=$idchef";
    $resultado = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($resultado);
    extract($fila);
    desconectar($c);
    return $category;
    // sin extract
//    return $fila["category"];
}


// Función que recibe un idchef y un code y devuelve un boolean indicando si es correcto
// true - si es ok
// false en caso contrario
function validarChef($idchef, $code) {
    $c = conectar();
    $select = "select idchef from chef where idchef=$idchef and code= $code";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
    return mysqli_num_rows($resultado) == 1;
    // El return es equivalente al if de abajo
//    if (mysqli_num_rows($resultado) == 1) {
//        return true;
//    } else {
//        return false;
//    }
}


// Función que cierra una conexión
function desconectar($conexion) {
    mysqli_close($conexion);
}

// Función que se conecta a una base de datos (school)
function conectar() {
    // conectamos a la bbdd (nos devuelve una conexión)
    $conexion = mysqli_connect("localhost", "root", "root", "cooking");
    // si no ha podido conectar devuelve null, comprobamos
    if (!$conexion) {
        // Acabamos el programa dando msg de error
        die("No se ha podido establecer la conexión con el servidor");
    }
    // si todo ha ido ok devolvemos la conexión
    return $conexion;
}



