<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
session_start();
require_once "bbdd.php";
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cooking manager</title>
    </head>
    <body>
        <h1>Bienenido a la aplicación del Restaurante StucomDinner</h1>
        <p>Login</p>
        <form method="POST">
            <p>Número de chef: <input type="number" name="chef" required></p>
            <p>Código de acceso: <input type="password" name="code" required></p>
            <input type="submit" name="login" value="Entrar">
        </form>
        <a href="registro.php">Registrarse</a>
        <?php
        if (isset($_POST["login"])) {
            $chef = $_POST["chef"];
            $code = $_POST["code"];
            if (validarChef($chef, $code)) {
                // Guardamos el id del usuario validado en la session
                $_SESSION["chef"] = $chef;
                // Necesitamos la categoría del chef
                $tipo = getCategoryById($chef);
                // guardamos la categoría en una variable de session
                $_SESSION["tipo"] = $tipo;
                if ($tipo == "Jefe") {
                    // Regidirimos a la página del jefe
                    header("Location: homeboss.php");
                } else {
                    // Redirigimos a la página  del chef
                    header("Location: homechef.php");
                }
            } else {
                echo "Usuario o contraseña incorrecta";
            }
        }
        ?>
    </body>
</html>
